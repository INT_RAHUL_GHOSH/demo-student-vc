import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Socket } from 'ng-socket-io';

@Injectable()
export class SocketSvc {

	constructor(private socket: Socket) { }

	socketConnect() {
		return new Promise((resolve) => {
			var conn = this.socket.connect();
			resolve(conn);
		});
	}

	socketDisconnect() {
		this.socket.disconnect();
	}

	socketRemoveAllListeners() {
		this.socket.removeAllListeners();
	}

	socketRemoveAudienceListeners() {
		console.log("Inside Audience Listener Remove");

		this.socket.removeAllListeners('message');
		this.socket.removeAllListeners('switching');
		this.socket.removeAllListeners('restartViewer');
		this.socket.removeAllListeners('sessionActive');
		this.socket.removeAllListeners('sessionInactive');
		this.socket.removeAllListeners('startStudentVideo');
		this.socket.removeAllListeners('getAllowOtherStudenttoView');
		this.socket.removeAllListeners('removeRaiseVideoViewer');
		this.socket.removeAllListeners('receive_message');
		this.socket.removeAllListeners('share_file');
		this.socket.removeAllListeners('ErrorMsg');
		this.socket.removeAllListeners('trainerRecievedFeed');
	}

	socketRemoveGoLiveListeners() {
		console.log("Inside Go Live Listener Remove");

		this.socket.removeAllListeners('userJoined');
		this.socket.removeAllListeners('webLoginStatus');
		this.socket.removeAllListeners('webLoggedOut');
		this.socket.removeAllListeners('webRegisterSuccess');
	}

	/* ####### GET Socket Messages From Server ############# */

	getMessage() {
		return this.socket
			.fromEvent<any>("message")
			.map(data => data);
	}

	getUserJoined() {
		return this.socket
			.fromEvent<any>("userJoined")
			.map(data => data);
	}

	getWebLoginStatus() {
		return this.socket
			.fromEvent<any>("webLoginStatus")
			.map(data => data);
	}

	getWebLoggedOut() {
		return this.socket
			.fromEvent<any>("webLoggedOut")
			.map(data => data);
	}

	getWebRegisterSuccess() {
		return this.socket
			.fromEvent<any>("webRegisterSuccess")
			.map(data => data);
	}

	getSwitching() {
		return this.socket
			.fromEvent<any>("switching")
			.map(data => data);
	}

	getRestartViewer() {
		return this.socket
			.fromEvent<any>("restartViewer")
			.map(data => data);
	}

	getSessionActive() {
		return this.socket
			.fromEvent<any>("sessionActive")
			.map(data => data);
	}

	getSessionInactive() {
		return this.socket
			.fromEvent<any>("sessionInactive")
			.map(data => data);
	}

	getConnectionStatus() {
		return this.socket
			.fromEvent<any>("connected")
			.map(data => data);
	}

	getStudentRaiseHandVideo() {
		return this.socket
			.fromEvent<any>("startStudentVideo")
			.map(data => data);
	}

	getAllowOtherStudenttoView() {
		return this.socket
			.fromEvent<any>("AllowOtherStudenttoView")
			.map(data => data);
	}

	getRemoveRaiseVideoViewer() {
		return this.socket
			.fromEvent<any>("removeRaiseVideoViewer")
			.map(data => data);
	}

	getChatMessage() {
		return this.socket
			.fromEvent<any>("receive_message")
			.map(data => data);
	}

	getFile() {
		return this.socket
			.fromEvent<any>("share_file")
			.map(data => data);
	}

	getViewerJoined() {
		return this.socket
			.fromEvent<any>("viewerJoined")
			.map(data => data);
	}

	getViewerLeft() {
		return this.socket
			.fromEvent<any>("viewerLeft")
			.map(data => data);
	}

	getRaisingHand() {
		return this.socket
			.fromEvent<any>("raisinghand")
			.map(data => data);
	}

	getWithdrawRaisingHand() {
		return this.socket
			.fromEvent<any>("withdrawraisinghand")
			.map(data => data);
	}

	getStartPresenterAsViewer() {
		return this.socket
			.fromEvent<any>("StartPresenterAsViewer")
			.map(data => data);
	}

	getRaiseHandStopped() {
		return this.socket
			.fromEvent<any>("raiseHandStopped")
			.map(data => data);
	}

	getTrainerRecievedFeed() {
		return this.socket
			.fromEvent<any>("trainerRecievedFeed")
			.map(data => data);
	}


	getErrorMsg() {
		return this.socket
			.fromEvent<any>("ErrorMsg")
			.map(data => data);
	}
	/*  / ####### GET Socket Messages From Server ############# */


	/* ####### EMIT Socket Messages to Server ############# */
	sendMessage(msg: any): void {
		this.socket
			.emit("message", msg);
	}

	sendLogin(msg: any): void {
		this.socket
			.emit("loggedIn", msg);
	}

	sendCheckWebLogin(msg: any): void {
		this.socket
			.emit("checkWebLogin", msg);
	}

	sendRegDeviceSocket(msg: any): void {
		this.socket
			.emit("regDeviceSocket", msg);
	}

	sendAppLogout(msg: any): void {
		this.socket
			.emit("appLogout", msg);
	}

	sendViewerLeft(msg: any): void {
		this.socket
			.emit("viewerLeft", msg);
	}

	sendRaiseHand(msg: any): void {
		this.socket
			.emit("raisehand", msg);
	}

	sendWithdrawRaiseHand(msg: any): void {
		this.socket
			.emit("withdrawraisehand", msg);
	}

	checkActivePresenter(msg: any): void {
		this.socket
			.emit("checkActivePresenter", msg);
	}

	sendChatMessage(msg: any): void {
		this.socket
			.emit("send_message", msg);
	}

	sendFile(msg: any): void {
		this.socket
			.emit("share_file_mobile", msg);
	}

	sendAllowOtherUser(msg: any): void {
		this.socket
			.emit("allowOtherUser", msg);
	}

	sendDisallowOtherUser(msg: any): void {
		this.socket
			.emit("disallowOtherUser", msg);
	}

	sendSessionEnded(msg: any): void {
		this.socket
			.emit("sessionEnded", msg);
	}

	sendDisconnect(msg: any): void {
		console.log("Inside Send Disconnect");
		this.socket
			.emit("pageClose", msg);
	}
	/*  / ####### EMIT Socket Messages to Server ############# */
}
