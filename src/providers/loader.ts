import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';

@Injectable()
export class loaderProvider {
  constructor(public loadingCtrl: LoadingController) {
  }

	/**
	 * Show and hide Loading
	 */
  loader: any = false;

  busyOn(text?: string) {

    let showLoader = () => {
      this.loader = this.loadingCtrl.create({
        content: text,
        //duration: 3000
      });
      this.loader.present();
    }

    if (this.loader) {
      this.busyOff();
      showLoader();
    } else {
      showLoader();
    }

  }

  busyOff() {
    if (this.loader) {
      this.loader.dismiss();
      this.loader = false;
    }
  }
}
