import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Events, App, ModalController, Content, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';

import { SocketSvc } from '../../providers/socket.service';
import { loaderProvider } from '../../providers/loader';

import * as kurentoUtils from 'kurento-utils';

var webRtcPeer;
var webRtcPeerLocal;
var webRtcPeerOtherViewer;
declare var window: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  localVideo: any = null;
  remoteVideo: any = null;

  subject_id: any = 25;
  user_id: any = 130;
  user_fullname: any = "Thomas Dsouza";

  endPointType: any = 'video';

  haveAllPermission: boolean = false;
  activePresenter: boolean = false;
  sessionJoined: boolean = false;

  @ViewChild(Content) content: Content;
  @ViewChild('remoteVid') remoteVideoElement: HTMLVideoElement;
  constructor(
    public app: App,
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    public modalCtrl: ModalController,
    public loader: loaderProvider,

    public statusBar: StatusBar,
    public platform: Platform,
    public socket: SocketSvc,
  ) {
    loader.busyOn("Loading");

    /* Check for Permissions */
    this.validateRequiredPermission()
      .then(() => {

        var loginDetails = {
          subjectId: this.subject_id,
          userType: "Student",
          userId: this.user_id,
          name: this.user_fullname
        }
        this.socket.sendLogin(loginDetails);
        //this.ownVideo(); // Start Your Video
      }, () => {
        console.log("Error Getting Permissions");
      })

    socket
      .getUserJoined()
      .subscribe(message => {
        console.log("User Joined =>", this.socket);
        this.checkActivePresenter(); // check active presenter
        loader.busyOff();
      });

		/*
		  Handling Socket 'Message' from Server
		  'On Message'
		*/
    socket
      .getMessage()
      .subscribe(message => {
        switch (message.id) {
          case 'presenterResponse':
            console.log("presenterResponse", message);
            if (message.response == 'accepted') {
              webRtcPeer.processAnswer(message.sdpAnswer);
            } else {
              this.dispose();
              loader.busyOff();
            }
            break;
          case 'viewerResponse':
            console.log("viewerResponse", message);
            if (message.response == 'accepted') {
              webRtcPeer.processAnswer(message.sdpAnswer);
              this.sessionJoined = true;
            } else {
              this.dispose();
              loader.busyOff();
            }
            break;
          case 'stopCommunication':
            this.dispose();
            break;
          case 'iceCandidate':
            console.log("Ice candidate", message);
            if (message.iceType) {
              webRtcPeerOtherViewer.addIceCandidate(message.candidate);
            } else {
              webRtcPeer.addIceCandidate(message.candidate);
            }
            break;
          default:
            console.log("Unrecognized Message : ", JSON.stringify(message));
        }
      });

		/*
		  Switch ScreenShare Feed with Video
		  'On switching'
		*/
    socket
      .getSwitching()
      .subscribe(message => {
        console.log(message);
        console.log("Viewer started");
        this.endPointType = message.mediaType;
        this.stop();
        console.log(this.endPointType);
        this.viewer();
      });

		/*
		  Screenshare Viewer Restart
		  'On restartViewer'
		*/
    socket
      .getRestartViewer()
      .subscribe(message => {
        this.endPointType = message.mediaType;
        setTimeout(() => {
          console.log("Viewer started", message);
          this.viewer();
        }, 1000);
      });

		/*
		  Presenter has Started Video Session - Default Session
		  'On sessionActive'
		*/
    socket
      .getSessionActive()
      .subscribe(message => {
        console.log('Presenter has Started Session');
        this.activePresenter = true;
      });

		/*
		  Presenter has Stopped Session - Reset All Variables
		  'On sessionInactive'
		*/
    socket
      .getSessionInactive()
      .subscribe(message => {
        if (this.activePresenter) {
          console.log('Presenter has STOPPED Session');
          this.activePresenter = false;
          this.sessionJoined = false;
          this.endPointType = 'video';
          this.stop();
          //this.ownVideo();
        }
      });

		/*
		  Error Handling
		  'On Error'
		*/
    socket
      .getErrorMsg()
      .subscribe(message => {
        console.log(message);
      });

  }

  /* Assign Local and Remote Video - Start Own Video Check Active Presenter */
  ionViewDidLoad() {
    this.localVideo = document.getElementById('localVideo');
    this.remoteVideo = document.getElementById('remoteVideo');
  }

  /* Show Tab Bar on Exiting Page */
  ionViewWillLeave() {
    this.stopVideoCall();
    this.socket.socketRemoveAudienceListeners();
    //this.disposeWebrtcPeerLocal();
    this.loader.busyOff();
  }

  /* Check for Permissions ** Function Defination ** */
  private validateRequiredPermission() {
    return new Promise((resolve, reject) => {
      window.plugins.k.webrtc.permission.check((success) => {
        if (success.responseCode == 0 && success.haveRequiredPermission) {
          this.haveAllPermission = true;
          console.log("all permission previously authorized");
          resolve();
        } else {
          window.plugins.k.webrtc.permission.request((success) => {
            if (success.responseCode == 0 && success.haveRequiredPermission) {
              this.haveAllPermission = true;
              console.log("all permission authorized");
              resolve();
            } else {
              this.haveAllPermission = false;
              reject();
            }
          }, (error) => {
            this.haveAllPermission = false;
            reject();
          });
        }
      }, (error) => {
        this.haveAllPermission = false;
        reject();
      });
    })
  }

  /* Start Own Video */
  ownVideo() {
    console.log("*** Starting Own Video ***");
    var options = {
      localVideo: this.remoteVideo,
      connectionConstraints: {
        offerToReceiveVideo: true,
        offerToReceiveAudio: true
      }
    }
    webRtcPeerLocal = kurentoUtils.WebRtcPeer.WebRtcPeerSendonly(options, (error) => {
      if (error) {
        console.log("Error =>", error);

      } else {

      }
    });
  }

  /* Join Video Call */
  joinVideoCall() {
    this.viewer('viewerJoin');
  }



	/*
	  Viewer Start Common Callback Function for Viewer Join and Restart for Screenshare
	  viewerjoin param for joining 1st time
	*/
  viewer(viewerJoin?: any) {
    //this.disposeWebrtcPeerLocal(); // Dispose Local WebRtc Peer for showing local video
    console.log("Inside ViewerJoin =>", viewerJoin);

    var viewerJoinedFlag;
    if (viewerJoin == 'viewerJoin') {
      viewerJoinedFlag = true;
    } else {
      viewerJoinedFlag = false;
    }

    if (!webRtcPeer) {
      var options = {
        remoteVideo: this.remoteVideo,
        localVideo: this.localVideo,
        connectionConstraints: {
          offerToReceiveAudio: true,
          offerToReceiveVideo: true
        },
        onicecandidate: (candidate) => {
          var message = {
            id: 'onIceCandidate',
            test: "This is test message",
            candidate: candidate
          }
          this.socket.sendMessage(message);
        }
      }
      webRtcPeer = kurentoUtils.WebRtcPeer.WebRtcPeerSendrecv(options, (error) => {
        if (error) {
          console.log(error);
        } else {
          webRtcPeer.generateOffer((error, offerSdp) => {
            if (error) {
              console.log(error);
            } else {
              var message = {
                id: 'viewer',
                sdpOffer: offerSdp,
                subject_id: this.subject_id,
                mediaType: this.endPointType,
                userId: this.user_id,
                fullName: this.user_fullname,
                firstJoin: viewerJoinedFlag
              }
              this.socket.sendMessage(message);
            }
          });
        }

      });
    }
  }

	/*
	  Check if there is any active presenter for current subject
	  If Active Presenter is Present - Show Join Video Call Button
	*/
  checkActivePresenter() {
    var message = {
      subject_id: this.subject_id
    };
    console.log("Check Active Presenter =>", message);

    this.socket.checkActivePresenter(message);
  }

  /* Stop Session - Reset all variables */
  stopVideoCall() {
    console.log("Stop Video Call Called =>", this.sessionJoined);
    if (this.sessionJoined) {
      var message = {
        name: this.user_fullname,
        subject_id: this.subject_id,
        user_id: this.user_id
      };
      this.socket.sendViewerLeft(message);
    } else {
      this.socket.sendDisconnect("test");
    }
  }

	/*
	  Common Function to Stop Call
	*/
  stopCall() {
    this.dispose();
  }

	/*
	  Callback function for disposing Webrtc Peer
	*/
  dispose() {
    if (webRtcPeer) {
      webRtcPeer.dispose();
      webRtcPeer = null;
    }
    if (webRtcPeerOtherViewer) {
      webRtcPeerOtherViewer.dispose();
      webRtcPeerOtherViewer = null;
    }
  }

  disposeWebrtcPeerLocal() {
    if (webRtcPeerLocal) {
      webRtcPeerLocal.dispose();
      webRtcPeerLocal = null;
      this.remoteVideo.src = null;
    }
  }

  disposeWebRtcPeerOtherViewer() {
    if (webRtcPeerOtherViewer) {
      webRtcPeerOtherViewer.dispose();
      webRtcPeerOtherViewer = null;
    }
  }

	/*
	  Callback Function for Stop
	*/
  stop() {
    console.log('Stop');
    if (webRtcPeer) {
      console.log('Stop');
      var message = {
        id: 'stop',
        subject_id: this.subject_id
      }
      this.socket.sendMessage(message);
      this.dispose();
      this.localVideo.removeAttribute("src");
    }
  }

	/*
		Video Started Playing 'onCanPlay'
	*/
  videoPlayingStarted() {
    console.log("Can Start Playing Remote Video");
    this.loader.busyOff();
  }


}
